{-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}

-- https://hspec.github.io/writing-specs.html
-- https://github.com/snoyberg/http-client/blob/master/TUTORIAL.md
-- https://www.oreilly.com/library/view/parallel-and-concurrent/9781449335939/ch07.html

import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.ByteString.Lazy as LBS
import Network.HTTP.Simple


import Control.Exception (evaluate)
import Test.Hspec
import Test.QuickCheck

import Control.Concurrent
import Control.Concurrent.Async

import Control.Monad
import Control.Monad.Cont (liftIO)
import System.IO
import qualified Utils.SysUtils as U

import Data.Aeson (FromJSON, ToJSON, decode, encode)
import GHC.Generics

import Controller.RunsHandler (mainHandler)
import qualified Model.GPSCoord as GPS


data ArrOfStrings =
  Strings [String]
  deriving (Generic, ToJSON, FromJSON)

fff :: LBS.ByteString
fff = "[\"asd\", \"lol\"]"

bsToArr :: LBS.ByteString ->  Maybe ArrOfStrings
bsToArr = decode

maybeToStrings :: Maybe ArrOfStrings -> [String]
maybeToStrings Nothing = []
maybeToStrings (Just (Strings xs)) = xs


--
-- :load test/integration/Spec.hs 

getHttpDays :: IO LBS.ByteString
getHttpDays = do
  webHost <- U.getEnvVar "WEB_HOST" "http://127.0.0.1"
  webPort <- U.getEnvVar "WEB_PORT" "9999"    
  response <- httpLBS (parseRequest_ ("GET " ++ webHost ++ ":" ++ webPort ++ "/days"))
  return $ getResponseBody response


checkGetHttpDays :: IO [String]
checkGetHttpDays = do
  hSetBuffering stdout NoBuffering
  forkIO mainHandler
  threadDelay (10 ^ 5)
  list <- getHttpDays
  let 
    ms = bsToArr list
    listOfDays = maybeToStrings ms
   in do
    mapM_ putStrLn listOfDays
    return listOfDays

main :: IO ()
main =
  hspec $
    describe "Prelude.head" $ 
      it "GET /days should return 10 elements" $ 
        do
          xs <- checkGetHttpDays
          length xs `shouldBe` 10
        
      -- it "returns the first element of an *arbitrary* list" $
      --   property $ \x xs -> head (x : xs) == (x :: Int)
      -- it "throws an exception if used with an empty list" $ 
      --   evaluate (head []) `shouldThrow` anyException
