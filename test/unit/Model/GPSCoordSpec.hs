module Model.GPSCoordSpec (spec) where

import qualified Model.GPSCoord as G

import Test.Hspec
import Test.QuickCheck

doubleTo :: [Double] -> G.StepInfo
doubleTo [d1, d2, d3, d4, d5] = G.createStepInfo (show d1, show d2, show d3, show d4, show d5)
doubleTo [d1, d2, d3, d4] = G.createStepInfo (show d1, show d2, show d3, show d4, "")




spec :: Spec
spec = do
  describe "Prelude.head" $ do
    it "returns the first element of a list" $
      head [23 ..] `shouldBe` (23 :: Int)      
    it "returns the second element of a list" $
       ( [26 ..] !! 2)  `shouldBe` (28 :: Int)
  describe "Steps" $ do
      it "Step with not enough data will produce a WrongStep" $
       G.createStep [1, 2, 3] `shouldBe` G.WrongStep
      it "Step with 4 Double will produce a StepInfo with Nothing as timestamp" $
        G.createStep [1, 2, 3, 4] `shouldBe`
             G.StepInfo 1 2 3 4 Nothing
      it "Step with 4 Double will produce a StepInfo with a timestamp" $
        G.createStep [1, 2, 3, 4, 5] `shouldBe` G.StepInfo 1 2 3 4 (Just 5)
      it "Create StepInfo with Strings" $
        G.createStepInfo ("", "", "", "", "") `shouldBe` G.WrongStep 
      it "Create StepInfo with valid Double Strings" $
        G.createStepInfo ("2", "3", "4", "5", "6") `shouldBe` G.StepInfo 2 3 4 5 (Just 6)
      it "Create StepInfo with 5 valid Double Strings" $
        property $ \d1 d2 d3 d4 d5  -> doubleTo [d1, d2, d3, d4, d5] `shouldBe` G.StepInfo d1 d2 d3 d4 (Just d5)
      it "Create StepInfo with 4 valid Double Strings" $
        property $ \d1 d2 d3 d4 ->
          doubleTo [d1, d2, d3, d4] `shouldBe` G.StepInfo d1 d2 d3 d4 Nothing
        
           
       