function log(){
    echo $1 >>  logs/watch-logs.txt
}
mkdir -p "./logs"
inotifywait -m ./binary-output -e create -e moved_to |
    while read dir action file; do
        log "The file '$file' appeared in directory '$dir' via '$action' at $(date)"
        
        log 'docker restart alpine-app-and-launcher'
        docker restart alpine-app-and-launcher
        log "Done."
    done