echo $(docker ps | grep stack_alpine )
STACK_DOCKER_ID=$(docker ps | grep stack_alpine | awk '{print $1}')
echo "docker id: $STACK_DOCKER_ID"

# Inside docker ...

docker exec $STACK_DOCKER_ID  mkdir -p /app

# start from a clean situation: delete all but keep stack cache 
# delete all files but .stack and /root
docker exec $STACK_DOCKER_ID  sh -c "cd /app && find . -mindepth 1  -maxdepth 1 \! \( -name root -o -name .stack-work \) -exec rm -rf '{}' \;"

# start clean
docker exec $STACK_DOCKER_ID  sh -c "cd /app"

# copy all files inside /app
docker cp ../../app $STACK_DOCKER_ID:/app
docker cp ../../src $STACK_DOCKER_ID:/app
docker cp ../../test $STACK_DOCKER_ID:/app
docker cp ../../ChangeLog.md $STACK_DOCKER_ID:/app
docker cp ../../LICENSE $STACK_DOCKER_ID:/app
docker cp ../../package.yaml $STACK_DOCKER_ID:/app
docker cp ../../RunTracker.cabal $STACK_DOCKER_ID:/app
docker cp ../../RunTracker.iml $STACK_DOCKER_ID:/app
docker cp ../../Setup.hs $STACK_DOCKER_ID:/app
docker cp ../../README.md $STACK_DOCKER_ID:/app
docker cp ../../stack.yaml $STACK_DOCKER_ID:/app
docker cp ../../stack.yaml.lock $STACK_DOCKER_ID:/app

# install
docker exec $STACK_DOCKER_ID  sh -c "cd /app && stack install --system-ghc --fast"
# debug ls generated binary
docker exec $STACK_DOCKER_ID  sh -c "ls /root/.local/bin"

# copy it in binary-output folder
docker cp $STACK_DOCKER_ID:/root/.local/bin/RunTracker-exe ./binary-output/
docker-compose -f docker-compose-develop.yml build
docker restart alpine-app-and-launcher
