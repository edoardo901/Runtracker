#/bin/sh
binaryDir="/binary-output"

function log(){
    echo $1
    echo $1 >> /logs/latest-bin-launcher.txt
}

WAIT_TIME_SECONDS=2

function tryToRun() {  
  log "I am going to look for an exe to start in $binaryDir"
  DATE=$(date -u)
  log "#########    date: $DATE      ###########"

  if [ ! -d "$binaryDir" ]; then
    log "Directory: $binaryDir does not exist"
    return 1
  fi

  if [  -d "$binaryDir" ]; then
    log "Directory: $binaryDir exists, good, binary file to execute should be placed there"
  fi

  mkdir -p /logs

  latest=""
  for file in $binaryDir/*; do
    if [ -z $latest ]; then
       latest=$file
    fi
    [[ $file -nt $latest ]] && latest=$file
  done

  FILE=$latest
  if [ -f "$FILE" ]; then
    log "Latest file in /binary-output is: " $latest " running it"   
    chmod +x $latest

    log "Running $latest >>/logs/app-logs.log 2>&1"
    sh -c $latest 2>&1 1>>/logs/app-logs.txt 2>>/logs/app-logs.txt  &
    sleep infinity
  fi

  if [ ! -f "$latest" ]; then
    log "No file found in $binaryDir, trying again in $WAIT_TIME_SECONDS seconds"
    sleep $WAIT_TIME_SECONDS
    WAIT_TIME_SECONDS = $WAIT_TIME_SECONDS * 2
    if [ $WAIT_TIME_SECONDS -gt 120 ]; then
      WAIT_TIME_SECONDS = 120
    fi

    tryToRun
  fi
}

DATE=$(date -u)
log "#########    date: $DATE      ###########"
log "I am going to look for an exe to start in $binaryDir"
tryToRun  >> /logs/latest-bin-launcher.txt 2>&1