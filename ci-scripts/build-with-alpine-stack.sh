echo "Building script - Pre checks "
echo "Where am I? $(pwd)"
echo "I can see ... , I see: " $(ls)
echo "Can I use stack? " $(which stack)

mkdir -p output  # for built exe, stored as artifact
mkdir -p root
echo "setting STACK_ROOT AS `pwd`/root"
export STACK_ROOT=`pwd`/root
echo "STACK_ROOT: $STACK_ROOT"
echo "ls in STACK_ROOT $(ls $STACK_ROOT)"
echo "---------------  BUILD COMMAND -------------"

echo "stack install --system-ghc --fast"
stack install --system-ghc --fast

RUNTRACK_BINARY_FULLPATH=""
for file in "/root/.local/bin"/*
do
  RUNTRACK_BINARY_FULLPATH="$file"
done

RUNTRACK_NAME=$(basename $RUNTRACK_BINARY_FULLPATH)
echo "RUNTRACK_NAME:  $RUNTRACK_NAME"

echo '  RUNTRACK_BINARY_FULLPATH:  ' $RUNTRACK_BINARY_FULLPATH
echo "copying $RUNTRACK_BINARY_FULLPATH in `pwd`/output"

version=$(cat ./package.yaml | grep version | awk '{print $2}')
echo "version: $version"

export CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)
ARTIFACT_NAME="$RUNTRACK_NAME-v.$version-$CI_COMMIT_SHORT_SHA"
echo "ARTIFACT_NAME::::    $ARTIFACT_NAME"
cp $RUNTRACK_BINARY_FULLPATH `pwd`/output/$ARTIFACT_NAME

echo "build end"



