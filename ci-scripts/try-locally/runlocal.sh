#!/bin/sh
cd ../..   # root of project
CACHEDIR=`pwd`/cache
gitlab-runner exec docker build-with-alpine-stack --cache-dir=$CACHEDIR  --docker-cache-dir=$CACHEDIR --docker-volumes=$CACHEDIR