# Editor choice and configuration

My (latest) personal choice is  **Visual Studio Code** with few Extensions

## Haskell Syntax Highlight
Official page https://marketplace.visualstudio.com/items?itemName=justusadam.language-haskell

No particular configuration is needed


## Simple GHC (Haskell) Integration
Official page: https://marketplace.visualstudio.com/items?itemName=dramforever.vscode-ghc-simple

My reccomended configuration:
- From inside the project
  - `stack build ghci`

- In Settings -> *Ghc Simple: Repl Command*: `stack ghci` 

## haskell-linter
My reccomended configuration:
- From outside the project
   - Check global stack resolver as describe Stack site https://docs.haskellstack.org/en/stable/yaml_configuration/ in order to install an updated version
   - `stack install hlint`
- In Settings -> Haskell › Hlint: Executable Path -> `hlint`

## hindent-format
Official page: https://marketplace.visualstudio.com/items?itemName=monofon.hindent-format
 
My reccomended configuration:

- From outside the project
   - Check global stack resolver as describe Stack site https://docs.haskellstack.org/en/stable/yaml_configuration/ in order to install an updated version
   - `stack install hindent`
- In settings 
    - ```json 
          "hindent-format.command": "hindent",
       ```
Usage:   cmd + shift + p   -> format document
