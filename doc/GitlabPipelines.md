## Automate the build (CI/CD)

To make compilation authomatic you can use
To test gitlab pipeline locally
https://medium.com/@umutuluer/how-to-test-gitlab-ci-locally-f9e6cef4f054

gitlab runner pipelines

### We all have Secretes

#### Gitlab variables
Settings -> CI/CD -> variables
For .pem, like the one for AWS you need `echo -e`
found here:

https://medium.com/@lucabecchetti/autodeploy-from-gitlab-to-multiple-aws-ec2-instances-a43448727c5a


**Local secrets**
put 
```
[[runners]]
  environment = ["DOCKER_AUTH_CONFIG={ \"auths\": { \"myregistryurl.com:port\": { \"auth\": \"add-the-token-from-docker\" } } }"]
```
in
`/Users/edoardo/.gitlab-runner/config.toml `

docker hub image is:
registry.hub.docker.com

found here:
https://stackoverflow.com/questions/51814625/gitlab-docker-auth-config-not-working/51836923

### Trying the Gitlab pipeline locally - tips
Execute the same docker image as the one executed via gitlab pipeline

```shell script
docker run -it  -v `pwd`:/myApp --rm  <my-image> sh
```

In this way I execute the image and I link the current folder with 
"myApp" folder inside the docker, so I have the same files available.


Once "inside" the container I can try to execute the same scripts that will be
executed inside Gitlab.

*caveats*

Some scripts require a private key, 
```shell script
export PRIVATE_KEY=$(cat myAwsUser.pem) 
sh script-to-deploy.sh
```

Some script assume you have built a file, for that I just
do
```shell script
echo "pretend to be a built file"  > /root/binary.output.exe
```