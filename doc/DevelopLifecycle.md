# Get it Up and Running

There are 3 ways to execute Runtracker project for 3 different development phases:
- For daily development: on your computer, natively
- For testing pre-deploying it: inside an Alpine Docker, 
- Deploying it in "production": inside an Alpine Docker in an AWS machine

![dev](./images/dev-cycle-png.png)

## Daily development
In this way you will run Runtracker project in your operating system, natively
  1. Redis installed locally or run in docker-compose

        From: `test/manual-test/`

        ```bash     
        docker-compose -f docker-compose-develop-simple.yml up -d
        ```

  2. building with Stack 
      
        From project root: 
        ```
        stack build
        ```

  3. running in REPL interactive mode 
      ```
       stack ghci
      ``` 
  4. inside ghci run: 
      ```
      main
      ```
  5. execute REST Api in Postman to try the webapp
  
## Testing pre-Deploy  
 In this way you will use a Alpine Docker image, with Stack installed, to compile the project, then you will run it inside an Alpine Docker container with just some basic static libraries.

Credits for the Alpine with Stack Dockerfile to andrevdm_reddit comment https://www.reddit.com/r/haskell/comments/bxggkz/ghc_builds_for_alpine_linux/ 

I updated the stack version and tagged the image as `edoardo90/alpine-stack-231`
and push it to docker hub with:
`docker pull`

  To build a Docker Image with Stack installed, you can use `DockerfileAlpine`: it will download and configure all the needed tools

  1. To create the image         
       from `manual-test`:

        ```sh
          docker build -f DockerfileAlpine -t stack_alpine .
        ```
        If you run `docker images` you will see this new image being created

  2.   Now you can run docker-compose to setup the environment useful to try
        the Alpine-compatbile binary. This is the same file that will be deployed. 
        The  docker-compose creates a configuration that is close to the one it will be present on the AWS machine where the "production" is hosted (see next section).

        From `manual-test`:
                    
          ```
            docker-compose -f docker-compose-develop.yml build alpine_and_launcher
            docker-compose -f docker-compose-develop.yml up -d
          ```
        If you run `docker ps` you should see docker container for Redis, Alpine, Alpine with Stack

        The Webapp container will be able to comminicate with the Redis DB because Docker will place them in the same network, so it will be able to use the Service name.
        It will connect to: `redis:default-port` and docker-compose will resolve this address.


  3. Run `update_and_build.sh`: this will copy all files from the root folder inside the docker, will build the project and will copy the output binary in `binary-output` folder


## Deploy on AWS
To expose Runtracker to the world, to its only user, I picked an AWS ts-micro (free-tier) machine in which I installed Docker and Docker-compose
https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:sort=desc:publicIp

The main difference between this and the previous mode, is that in the VM are executed just 2 container: one for Alpine and another one for Redis.
Moreover there is a little bash script that looks for new files: `watch-and-place.sh`, it is based on `inotify-tools`, ubuntu tool
(https://howtoinstall.co/en/ubuntu/trusty/inotify-tools).

This script will watch `binary-output` folder and launch a `docker-restart`command when a new file appears, so when a new version of Runtracker is deployed.


## Watch logs on EC2 machine
Just a bash script that read logs on `alpine-app-and-launcher`, in order to keep reading logs even after the Docker container reboot
```shell script
while true
do
  sleep 1
  docker logs  -f alpine-app-and-launcher
done
```