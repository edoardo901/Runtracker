
## Impure time
In backend I want to save every run session using the 
timestamp in which it started as key.
For instance if I go outside running today I want to save in DB (Redis)
all the steps statistics using the day and the hour I'm going out as a key

In Haskell, since "what time is it now?" is not a math function, called in different times it helds 
different values, so it is tagged with IO
```haskell
import Data.Time

stripSpace :: String -> String
stripSpace = map (\c -> if c==' ' then '-'; else c)

main = do
  t <- getCurrentTime
  print (stripSpace ( show t) )
```