module Utils.Logger
  ( printlog
  ) where

import Data.Maybe (fromMaybe)
import System.Environment (lookupEnv)
import Utils.SysUtils (getEnvVar)

-- >>> show "asd"

printlog :: String -> IO ()
printlog a = do
    print a
    logDir <- getEnvVar "LOGS_FILE" "logs.txt"
    appendFile logDir $ "\n" ++ a