module Utils.StringConventions

  ( getToday
  , stripSpace
  , printTime
  , getHMS
  ) where

import Data.Time
import Data.Time.Calendar

import Data.Time.Clock
import Data.Time.LocalTime

getHMS :: IO String
getHMS = do
  now <- getCurrentTime
  timezone <- getCurrentTimeZone
  let (TimeOfDay hour minute second) =
        localTimeOfDay $ utcToLocalTime timezone now
  return $ show hour ++ ":" ++ show minute ++ ":" ++ show second 


-- >>> getToday
-- "2020/6/1"
getToday :: IO String -- :: (year,month,day)
getToday = do
  t <- getCurrentTime
  let (y, m, g) = (toGregorian . utctDay) t
   in return (show y ++ "/" ++ show m ++ "/" ++ show g)


stripSpace :: String -> String
stripSpace =
  map
    (\c ->
       if c == ' '
         then '-'
         else c)

printTime :: IO String
printTime = stripSpace . show <$> getCurrentTime
