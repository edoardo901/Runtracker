module Utils.SysUtils (getEnvVar) where
import Data.Maybe (fromMaybe)
import System.Environment (lookupEnv)
getEnvVar :: String -> String -> IO String
getEnvVar name base = do
  v <- lookupEnv name
  return (fromMaybe base v)
