{-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}

module Model.GPSCoord
  ( createStepInfo
  , createStep
  , createJSONTextList
  , JSONTextList
  , lengtOfTextList
  , Step
  , StepInfo(..)
  , stepToJson'
  , stringsToStepInfo
  ) where

import Data.Maybe (catMaybes)
import qualified Text.Read as R

import Data.Aeson (FromJSON, ToJSON, decode, encode)
import GHC.Generics

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LB
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Encoding as E

data StepInfo
  = StepInfo
      { lat :: Double
      , lon :: Double
      , speed :: Double
      , accuracy :: Double
      , timeStamp :: Maybe Double
      }
  | WrongStep
  deriving (Generic, ToJSON, FromJSON, Show, Eq)

exampleStep :: StepInfo
exampleStep = StepInfo 9.2 40.12 0 0 Nothing

exampleWrongStep :: StepInfo
exampleWrongStep = WrongStep

exampleStep' :: StepInfo
exampleStep' =
  StepInfo
    { lat = 9.3
    , lon = 40.11
    , speed = 0
    , accuracy = 1
    , timeStamp = Just 159186324594
    }

type Step = (String, String, String, String, String)

exampleStepStrings :: Step
exampleStepStrings = ("9.2", "40.11", "0", "1", "159186324594")

createStepInfo :: Step -> StepInfo
createStepInfo (lat, lon, speed, acc, ts) =
  let ms :: [Maybe Double]
      ms = R.readMaybe <$> [lat, lon, speed, acc, ts]
      ds = catMaybes ms
   in createStep ds

exampleStepDouble :: [Double]
exampleStepDouble = [9.2, 40.11, 0, 1, 159186324594]

exampleStepDoubleBAD :: [Double]
exampleStepDoubleBAD = [9.2]

createStep :: [Double] -> StepInfo
createStep [lat, lon, speed, acc] =
  StepInfo
    {lat = lat, lon = lon, speed = speed, accuracy = acc, timeStamp = Nothing}
createStep [lat, lon, speed, acc, ts] =
  StepInfo
    {lat = lat, lon = lon, speed = speed, accuracy = acc, timeStamp = Just ts}
createStep _ = WrongStep

data JSONTextList =
  JSONTextList [LT.Text]
  deriving (Generic, ToJSON, Show)

lengtOfTextList :: JSONTextList -> Int
lengtOfTextList (JSONTextList l) = length l

data ArrOfStrings =
  Strings [String]
  deriving (Generic, ToJSON, FromJSON)

type DataToSave = [String]

createJSONTextList :: [BS.ByteString] -> JSONTextList
createJSONTextList bss =
  let lbs :: [LB.ByteString]
      lbs = (LB.fromStrict <$> bss)
      tss :: [LT.Text]
      tss = E.decodeUtf8 <$> lbs
   in JSONTextList tss

stepToJson :: StepInfo -> LB.ByteString
stepToJson = encode

stepToJson' :: StepInfo -> BS.ByteString
stepToJson' = LB.toStrict . encode

stringsToStepInfo :: [BS.ByteString] -> [StepInfo]
stringsToStepInfo bss =
  let mss :: [Maybe StepInfo]
      lbs :: [LB.ByteString]
      lbs = map LB.fromStrict bss
      mss = map decode lbs
      ss = catMaybes mss
   in ss
