{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Dao.RedisHelper
  ( getStepsFromList
  , saveStepInList
  , getListOfDays
  ) where

import Control.Monad.Trans (liftIO)
import Data.Aeson
import Database.Redis as DB

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy as LB
import qualified Data.Text.Lazy as LT

import System.Environment (lookupEnv)

import qualified Model.GPSCoord as GPS
import qualified Utils.Logger as LOG
import qualified Utils.SysUtils as SYS

type ListName = String

type PayloadList = [B.ByteString]

allKeys :: Redis (Either Reply PayloadList)
allKeys = DB.keys "*"

rangeByName :: ListName -> Redis (Either Reply PayloadList)
rangeByName listName = DB.lrange (B8.pack listName) 0 (-1)

saveInList ::  ListName -> PayloadList -> Redis (Either Reply Integer)
saveInList listName = DB.lpush $ B8.pack listName


getListOfDays :: IO GPS.JSONTextList
getListOfDays = do
  conn <- makeRedisConnection
  runRedis conn $ do
    res <- allKeys
    liftIO $ LOG.printlog $ "redis get outcome: " ++ describeResult res
    return $ GPS.createJSONTextList (payloadOrEmptyList res)

getStepsFromList :: ListName -> IO [GPS.StepInfo]
getStepsFromList lname = do
  conn <- makeRedisConnection
  runRedis conn $ do
    rangeResult <- rangeByName lname    
    let payload :: PayloadList
        payload = payloadOrEmptyList rangeResult
     in return (GPS.stringsToStepInfo payload)

saveStepInList :: ListName -> GPS.Step -> IO ()
saveStepInList listName (lat, lon, spd, acc, ts) = do
  conn <- makeRedisConnection
  let stepInfo  = GPS.createStepInfo (lat, lon, spd, acc, ts)
      steps :: PayloadList
      steps = [GPS.stepToJson' stepInfo]
   in runRedis conn $ do
        result <- saveInList listName steps
        liftIO $ LOG.printlog $ "redis save outcome: " ++ describeResult result

describeResult :: Show a => Either Reply a -> String
describeResult (Left reply) = "bad response from Redis: " ++ show reply
describeResult (Right n) = "good response from Redis "

payloadOrEmptyList :: Either Reply PayloadList -> PayloadList
payloadOrEmptyList (Left reply) = []
payloadOrEmptyList (Right bsPayload) = bsPayload

makeRedisConnection :: IO Connection
makeRedisConnection = do
  host <- SYS.getEnvVar "REDIS_HOST" "127.0.0.1"
  port <- SYS.getEnvVar "REDIS_PORT" "6379"
  LOG.printlog $ "REDIS_PORT:" ++ port
  LOG.printlog $ "REDIS_HOST:" ++ host
  let rPort :: Int
      rPort = read port
   in connect (overrideDefaultConn host rPort)

overrideDefaultConn :: String -> Int -> ConnectInfo
overrideDefaultConn host port =
  ConnInfo
    { connectHost = host
    , connectPort = PortNumber (fromIntegral port)
    , connectAuth = Nothing
    , connectDatabase = 0
    , connectMaxConnections = 50
    , connectMaxIdleTime = 30
    , connectTimeout = Nothing
    , connectTLSParams = Nothing
    }
