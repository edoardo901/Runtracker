{-# LANGUAGE OverloadedStrings #-}

module Controller.RunsHandler
  ( mainHandler
  ) where

import Control.Monad.Cont (liftIO)
import qualified Dao.RedisHelper as RH
import Data.String (fromString)
import Data.List (intercalate)
import Data.Text.Lazy (Text, pack, unpack)

import qualified Model.GPSCoord as GPS
import Network.HTTP.Types
import Utils.Logger as L
import Utils.StringConventions
import qualified Utils.SysUtils as U
import Web.Scotty

mainHandler = do
  print "starting..."
  port <- U.getEnvVar "WEB_PORT" "9999"
  logDir <- U.getEnvVar "LOGS_FILE" "logs.txt"
  redisHost <- U.getEnvVar "REDIS_HOST" "127.0.0.1"
  timeHMS <- getHMS
  let loh =
        [ "time:" ++ timeHMS
        , "logging in: " ++logDir
        , "redis host:" ++ redisHost
        , "using port:" ++ port
        ]
  let webPort :: Int
      webPort = read port
   in do liftIO $ L.printlog (intercalate "\n" loh)
         scotty webPort $ do
           get "/gps" getAndSaveGPSAction
           get "/steps" getStepListAction
           get "/days" getListOfDays

getListOfDays :: ActionM ()
getListOfDays = do
  steps <- liftIO RH.getListOfDays
  liftIO $ print (" Got:" ++ show (GPS.lengtOfTextList steps) ++ " steps")
  json steps

getStepListAction :: ActionM ()
getStepListAction = do
  liftIO $ print "getting params.."
  utcDay <- param "from" `rescue` (\_ -> return "0")
  liftIO $ print (" utc Day: " ++ unpack utcDay)
  steps <- liftIO $ RH.getStepsFromList (unpack utcDay)
  liftIO $ print "Done"
  json steps

getAndSaveGPSAction :: ActionM ()
getAndSaveGPSAction = do
  liftIO $ print "getting params.."
  lat <- param "lat" `rescue` (\_ -> return "")
  lon <- param "lon" `rescue` (\_ -> return "")
  speed <- param "speed" `rescue` (\_ -> return "0")
  ts <- param "ts" `rescue` (\_ -> return "")
  accuracy <- param "accuracy" `rescue` (\_ -> return "0")
  date <- liftIO getToday
  liftIO $ RH.saveStepInList ("run" ++ date) (lat, lon, speed, accuracy, ts)
  text $ pack ("done: saved with key: run" ++ date)
