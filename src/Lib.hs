module Lib
    ( someFunc,summy
    ) where

summ :: [Int] -> Int
summ xs = let x    =  2 
              y =  4
       in 333

f    ::   IO ()
f = do
      x <- readLn
      putStrLn ("ciao"  ++ x)

summy :: Int -> Int -> Int
summy = (+)

someFunc :: IO ()
someFunc = putStrLn "someFunc"
