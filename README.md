# RunTracker
Runtracker is an Application to track movements during a run.
For instance I want to see the map of my run with different speeds
over my path, maybe in google maps.
This project is the Backend of it.

### Backend:
![be](./doc/images/Runtracker%20Backend.png)

### Frontend:
![fe](./doc/images/Runtracker%20Frontend.svg)


# Life Cycle
I want to develop a Haskell project locally, in Visual Studio Code, with all the plugins I need, try it, and then deploy it in a light version, on a light Alpine based Docker image.

To do so the main idea is to compile a Stack Haskell Project inside a Docker container based on Alpine in order to have binaries compatible for this minimal Docker-oriented Operating System, and run them on any machine with Docker installed. 

For instance, in my case I used an EC2 AWS machine


# Glossary
- Haskell: purely functional programming language
- GHC: Haskell Compiler
- Stack:  Build manager for haskell (similar to npm for Node.js, or Maven for Java)
- Project root: the folder containing `stack.yaml`, the one you get when you clone the project

# Building and Running prerequisites
- Docker  (https://www.docker.com/)
- Docker Compose (https://docs.docker.com/compose/install/)
- Stack (https://docs.haskellstack.org/en/stable/README/)
- nice to have: Postman (https://www.postman.com/) or any other tool to try REST APIs
- optional: Redis (https://redis.io/) otherwise you can use the Docker version of it

# Quick start
Install Docker and Stack, run the following from the Project root:
```
docker-compose -f test/manual-test/docker-compose-develop-simple.yml up -d
stack build  --fast
curl -X GET 'http://127.0.0.1:9999/gps?lat=11.99891&lon=44.912394829584'
```
Congratulations! You have entered your first "step" in your Runtracker Database



# Development Lifecycle
Detailed info on how to build and run Runtracker here: [Development Lifecycle](./doc/DevelopLifecycle.md)


# CI/CD
[CI/CD](./doc/GitlabPipelines.md)